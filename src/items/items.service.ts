import { Injectable } from '@nestjs/common';
import { Item } from './interfaces/item.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class ItemsService {
  constructor(@InjectModel('Item') private readonly itemModel: Model<Item>) {}

  async findAll(): Promise<Item[]> {
    return await this.itemModel.find();
  }

  async findOne(id: string): Promise<Item> {
    return await this.itemModel.findOne({ _id: id });
  }

//   async create(item: Item): Promise<Item> {
//     const newItem = new this.itemModel(item);
//     return await newItem.save();
//   }
async create(item:Item):Promise<Item>{
    const newItem=new this.itemModel(item);
    return await newItem.save();
}

//   async delete(id: string): Promise<Item> {
//     return await this.itemModel.findByIdAndRemove(id);
//   }
async delete(id:string):Promise<Item>{
    return await this.itemModel.findByIdAndRemove(id);
}

//   async update(id: string, item: Item): Promise<Item> {
//     return await this.itemModel.findByIdAndUpdate(id, item, { new: true });
//   }
async update(id:string,item:Item):Promise<Item>{
    return await this.itemModel.findByIdAndUpdate(id);
}

}



// import { Injectable } from '@nestjs/common';
// import {Item}from './interfaces/item.interface';
// import {Model} from 'mongoose';
// import {InjectModel} from '@nestjs/mongoose';
// import { from } from 'rxjs';
// @Injectable()
// export class ItemsService {
// //-------------
// constructor(@InjectModel('Item')private readonly  itemModule:Model<Item>   ){}

//---array------------    
// private readonly items:Item[]=[
//     {
//         id:"4345345",
//         name:"poriyadaliry",
//         cost:200,
//         description:"this is test for nestjs"
//     },
//     {
//         id:"6755345",
//         name:"mohamad baghjare",
//         cost:89,
//         description:"he is for seo engring"
//     },
//     {
//         id:"982659",
//         name:"fatameh hosini",
//         cost:3230,
//         description:"she is senior web devopers"
//     }
// ];
// async findAll():Promise<Item[]>{
//     return await this.itemModule.find();
// }
// async findOne(id:string):Promise<Item>{
// return await this.itemModule.findOne({_id:id});
// }

/// }
