import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Body,
    Param,
  } from '@nestjs/common';
  import { CreateItemDto } from './dto/create-item.dto';
  import { ItemsService } from './items.service';
  import { Item } from './interfaces/item.interface';
  
  @Controller('items')
  export class ItemsController {
    constructor(private readonly itemsService: ItemsService) {}
  
    @Get()
    findAll(): Promise<Item[]> {
      return this.itemsService.findAll();
    }
  
    @Get(':id')
    findOne(@Param('id') id): Promise<Item> {
      return this.itemsService.findOne(id);
    }
  
    // @Post()
    // create(@Body() createItemDto: CreateItemDto): Promise<Item> {
    //   return this.itemsService.create(createItemDto);
    // }
    @Post()
    create(@Body() createItemDto:CreateItemDto):Promise<Item>{
        return this.itemsService.create(createItemDto);
    }

    // @Delete(':id')
    // delete(@Param('id') id): Promise<Item> {
    //   return this.itemsService.delete(id);
    // }
    @Delete(':id')
    delete(@Param('id')id ):Promise<Item>{
        return this.itemsService.delete(id);
    }
  
    // @Put(':id')
    // update(@Body() updateItemDto: CreateItemDto, @Param('id') id): Promise<Item> {
    //   return this.itemsService.update(id, updateItemDto);
    // }
    @Put(':id')
    uupdate(@Body() updateItemDto:CreateItemDto,@Param('id')id):Promise<Item>{

        return this.itemsService.update(id,updateItemDto);
    }
  }
  
// import { Controller,Get ,Post,Put,Delete,Body,Req,Res, Param} from '@nestjs/common';
// import {create} from 'domain';
// import { from } from 'rxjs';
// import {CreateItemDto} from './dto/create-item.dto';
// // import {Request,Response} from 'express';
// import {ItemsService} from './items.service';
// import {Item} from './interfaces/item.interface';
// @Controller('items')
// export class ItemsController {
//     constructor(private readonly itemsService:ItemsService){}
//     @Get()
//      async findAll():Promise<Item[]>{
//      return this.itemsService.findAll();
     

//     }
    
//     // findAll(@Req() req:Request,@Res() res:Response):any{
//     //     res.send('this is test with res and req');

//     // }

//     @Get(':id')
//     async findOne(@Param('id') id ):Promise<Item>{
//     return this.itemsService.findOne(id);
//     }
//     // @Get(':id')
//     // findOne(@Param() param):string{
//     //     return `item number of ${param.id}`;

//     // }
//     @Post()
//     create(@Body() createItemDto:CreateItemDto) :string{
//         return `Name ${createItemDto.name} Description:${createItemDto.description}`;
//     }

//     @Delete(':id')
//     delete(@Param('id') id):string{
//         return `Delete item number ${id}`;
//     }

//     //have error
//     @Put(':id')
//     Update(@Body() updateItemdto:CreateItemDto, @Param('id') id):string{
//         return`Update ${id} -Name ${updateItemdto.name}`;
//     }
// }
