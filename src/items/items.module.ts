import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ItemsController } from './items.controller';
import { ItemsService } from './items.service';
 import {ItemSchema} from './interfaces/schemas/item.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Item', schema: ItemSchema }])],
  controllers: [ItemsController],
  providers: [ItemsService],
})
export class ItemsModule {}


// import { Module } from '@nestjs/common';
// import { ItemsController } from './items.controller';
// import { ItemsService } from './items.service';
// import {ItemSchema} from './interfaces/schemas/item.schema';
// import { from } from 'rxjs';
// import {MongooseModule} from '@nestjs/mongoose';
// @Module({
//   imports: [MongooseModule.forFeature([{name:'Items',schema:Ite}])],
//   controllers: [ ItemsController],
//   providers: [ ItemsService],
// })
// export class ItemModule {}